This module creates a context reaction, that takes a string a parameter (a Gemius code).

Using the context module, it can be invoked on any page and every condition can have it's own Gemius Code. 

A xgemius.js file i required from Gemius. In the admin section for the module it can be configured where the file is locate.

This module will add both the required JavaScript for the Gemius code and the include for the xgemius.js file to the footer of the page.
