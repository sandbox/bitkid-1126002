<?php

function gemius_admin_settings() {
  $form = array(
    'xgemius_path' => array(
      '#title' => t('Path to xgemius file'),
      '#description' => t('The path to the xgemius file relative to drupal path'),
      '#type' => 'textfield',
      '#default_value' => variable_get('gemius_file_path', ''),
      '#required' => TRUE,
    ),
    '#submit' => array('gemius_admin_settings_submit'),
    '#validate' => array('gemius_admin_settings_validate'),
  );

  return system_settings_form($form);
}

function gemius_admin_settings_validate($form, &$form_state) {
  if (isset($form['xgemius_path']) && !empty($form['xgemius_path']['#value']) && file_exists(DRUPAL_ROOT . '/' . trim($form['xgemius_path']['#value'], '/'))) {
    return true;
  } else {
    form_set_error('xgemius_path', t('The file does not exist in this path'));
  }
}

function gemius_admin_settings_submit($form, &$form_state) {
  variable_set('gemius_file_path', trim($form['xgemius_path']['#value'], '/'));
}
