<?php

/**
 * Output context debug information.
 */ 
class gemius_context_reaction_gemiusreaction extends context_reaction {
  function options_form($context) {
    $gemiusform = array(
      'gemiuscode' => 
        array(
          '#type' => 'textfield', 
          '#title' => t('Gemius code'),
          '#default_value' => isset($context->reactions['gemius']['gemiuscode']) ? $context->reactions['gemius']['gemiuscode'] : '',
          '#description' => t('The code provided by Gemius, that should be used to identify these types of pages.'),
        ),
      'priority' =>
        array(
          '#type' => 'textfield',
          '#title' => t('Priority'),
          '#default_value' => isset($context->reactions['gemius']['priority']) ? $context->reactions['gemius']['priority'] : '',
          '#size' => '4',
          '#description' => t('If more than one condition adds a Gemius code to a page, the priority will decide which one to use.'),
        ),
    );
    return $gemiusform;
  }

  function options_form_submit($values) {
    return array('gemiuscode' => $values['gemiuscode'], 'priority' => $values['priority'] ?: 0);
  }

  /**
   * Output a list of active contexts.
   */
  function execute() {
    $contexts = context_active_contexts();
    $prio = 0;
    $gemiuscode = 0;

    foreach ($contexts as $context) {
      if (!empty($context->reactions['gemius'])) {
        if ($context->reactions['gemius']['priority'] >= $prio) {
          $prio = $context->reactions['gemius']['priority'];
          $gemiuscode = $context->reactions['gemius']['gemiuscode'];
        }
      }
    }
    
    if ($gemiuscode !== 0) {
      drupal_add_js('var pp_gemius_identifier = new String(\''. $gemiuscode .'\');', array('scope' => 'footer', 'type' => 'inline'));
      if (variable_get('gemius_file_path', '') != '') {
        $xgemius_path = base_path().variable_get('gemius_file_path', '');
      } else {
        $xgemius_path = drupal_get_path('module', 'gemius').'/xgemius.js';
      }
      drupal_add_js($xgemius_path, array('scope' => 'footer'));
    }
  }
} 

